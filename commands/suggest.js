module.exports = {
    name: 'suggest',
    description: 'Adds a question to the questions list',
    admin: false,
    execute(message, args){
        const Discord = require('discord.js');
        const config = require('../config.json');

        const question = args.join(' ');
        const time = new Date();

        const fs = require('fs');

        // returns a random colour from the input array
        function randomColour(colours) {
            return colours[Math.floor(Math.random() * colours.length)];
        }

        // makes sure the question isn't blank, if it is it sends a message
        // to the user
        if (question.trim() == '') {
            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle("Question not queued")
                .setDescription("Please make sure your question isn't just blank!")
            
            message.channel.send(errorEmbed);
            return;
        }

        // all of this just formats the times so that they have two digits
        // the only way I could figure out how to do this is to have this
        // entire mess (this looks disgusting please give me an alternative)
        date = (time.getDate()/10).toFixed(1).toString().replace('.', '');
        month = ((time.getMonth()+1)/10).toFixed(1).toString().replace('.', '');
        year = time.getFullYear().toString();
        hour = (time.getHours()/10).toFixed(1).toString().replace('.', '');
        minute = (time.getMinutes()/10).toFixed(1).toString().replace('.', '');
        timeString = `${date}/${month}/${year}, at ${hour}:${minute}`;

        // appends the question to the questions file in the format
        // <question>|<nickname of user that asked it>|<time it was asked> and then moves to the next line
        // This is in a try catch to make sure the user knows if the question didn't go through for whatever reason
        try {
            fs.appendFileSync(config.questionFile, `${question}|${message.member.displayName}|${timeString}|${message.author.id}\n`);

            confirmEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.colours))
                .setTitle('Question Suggestion')
                .setDescription(question)
                .setFooter('Thanks for the suggestion, it has been added to the queue!');
            
            message.channel.send(confirmEmbed);

            console.log(`Question asked by ${message.member.displayName}`);
        } catch {
            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle("Question not queued")
                .setDescription("There was an error queueing your question")
                .setFooter("Contact a mod for help, there's something wrong with me");
            
            message.channel.send(errorEmbed);
            console.log("Error logging question to questions file");
        }
        
    }
}