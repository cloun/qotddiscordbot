const Discord = require('discord.js');
const config = require('../config.json');

module.exports = {
    name: 'test',
    description: 'Sends a test question to the send channel in the config',
    admin: true,
    execute(message, args, client){
        
        // Attempts to create a variable that stores the channel to send the questions in
        // in a try catch just in case the id is entered incorrectly
        var sendChannel;

        try {
            sendChannel = client.channels.cache.get(config.sendChannel);
        } catch {
            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle(`**Could not find the send channel id:<#${config.sendChannel}>`)
                .setDescription("The id of the channel is likely inputted wrong in the config file")
            
            message.channel.send(errorEmbed);
            return;
        }
        
        // sends the question to the send channel
        questionEmbed = new Discord.MessageEmbed()
            .setColor(randomColour(config.colours))
            .setTitle("❔❓**Test Question**❓❔")
            .setDescription("This is a test, how is your day going?")
            .setFooter(`Requested by ${message.member.displayName}`);
    
        sendChannel.send(questionEmbed);
    }
}