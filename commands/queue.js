module.exports = {
    name: 'queue',
    description: 'Shows the current queue of questions with their respective indices',
    admin: false,
    execute(message, args){
        const Discord = require('discord.js');
        const config = require('../config.json');

        const fs = require('fs');

        // returns a random colour
        function randomColour(colours) {
            return colours[Math.floor(Math.random() * colours.length)];
        }
        
        // retrieves all the questions from the question file (and removes the last value = '')
        questions = fs.readFileSync(config.questionFile, 'utf-8').split('\n');
        questions.pop();

        // generates the embed with 10 listed questions
        const generateEmbed = start => {
            // slices the questions so it shows only 10
            const listQuestions = questions.slice(start, start+10)

        
            // creates the embed
            listEmbed = new Discord.MessageEmbed()
            .setColor(randomColour(config.colours))
            .setTitle("**Questions**")

            // adds each question to the embed
            for(i = 0; i < listQuestions.length; i++){
                question = listQuestions[i];

                questionArray = question.split('|');
                listEmbed.addField(`**[${i+start+1}]** ${questionArray[0]}`, `asked by ${questionArray[1]} on ${questionArray[2]}`);
            } 

            return listEmbed
        }
        
        
        const author = message.author

        message.channel.send(generateEmbed(0)).then (message => {
            // stops if there are less than 10 questions, no reason to have reacts
            if (questions.length <=10 ) return

            // first reacts with a right arrow
            message.react('➡️')
            
            const collector = message.createReactionCollector(
                //collects left and right arrow reactions from the user
                //that sent the message
                (reaction, user) => ['⬅️', '➡️'].includes (reaction.emoji.name) && user.id === author.id,
                //turns of the collector after a minute
                {time: 60000}
            )

            let currentIndex = 0
            collector.on('collect', reaction => {
                // removes existing reactions and then increases or decreases the index respectively
                message.reactions.removeAll().then(async () => {
                    reaction.emoji.name === '⬅️' ? currentIndex -=10 : currentIndex += 10

                    // updates the message to be the new embed
                    message.edit(generateEmbed(currentIndex))
                    // adds a left arrow if it isnt the start
                    // adds a right arrow if there are more questions left in the queue
                    if (currentIndex != 0) await message.react('⬅️')
                    if (currentIndex + 10 < questions.length) message.react('➡️')
                })
            })
        })
        

    }
}