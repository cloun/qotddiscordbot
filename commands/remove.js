const Discord = require('discord.js');
const config = require('../config.json');

module.exports = {
    name: 'remove',
    description: `Removes a selected question. Only the person who asked the question, and admins can remove a question\nUsage: ${config.prefix}remove <index>`,
    admin: false,
    execute(message, args){

        const fs = require('fs');

        // returns a random colour
        function randomColour(colours) {
            return colours[Math.floor(Math.random() * colours.length)];
        }

        
        // retrieves all the questions from the question file (and removes the last value = '')
        questions = fs.readFileSync(config.questionFile, 'utf-8').split('\n');
        questions.pop();

        var removedQuestion;

        try {
            removedQuestion = questions.splice(parseInt(args[0], 10) - 1, 1);
        } catch {
            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle('Error removing message')
                .setDescription(`Could not find index ${args[0]}`)
            
            message.channel.send(errorEmbed);
            return;
        }

        removedQuestionArray = removedQuestion[0].split("|")
        
        if (message.author.id.toString() == removedQuestionArray[3]) {
            fs.writeFileSync(config.questionFile, questions.join('\n') + '\n');

            confirmEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.colours))
                .setTitle("**Question Successfully Removed**")
                .setDescription(`\"${removedQuestionArray[0]} • Asked by ${removedQuestionArray[1]} • on ${removedQuestionArray[2]}\"`)
                .setFooter(`Removal requested by ${message.member.displayName}`);

            message.channel.send(confirmEmbed);

            console.log(`${message.member.displayName} successfully removed their own question`)

        } else if(message.member.hasPermission("ADMINISTRATOR")) {
            fs.writeFileSync(config.questionFile, questions.join('\n') + '\n');
            
            confirmEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.colours))
                .setTitle("**Question Successfully Removed**")
                .setDescription(`\"${removedQuestionArray[0]} • Asked by ${removedQuestionArray[1]} • on ${removedQuestionArray[2]}\"`)
                .setFooter(`Removal requested by ${message.member.displayName}`);
            
            message.channel.send(confirmEmbed);

            console.log(`${message.member.displayName} successfully removed a question with their admin permissions`);
        }
        else {
            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle("**Insufficient Permissions**")
                .setDescription("The question could not be removed because the user did not ask the question, and is not an admin")
                .setFooter(`Removal requested by ${message.member.displayName}`);
            
            message.channel.send(errorEmbed);
            
            console.log(`${message.member.displayName} requested a removal of a question with insufficient permissions`)
        }
    }
}