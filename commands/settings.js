const Discord = require('discord.js')
const config = require('../config.json')

module.exports = {
    name: 'settings',
    description: 'Lists some of the current config settings',
    admin: true,
    execute(message, args){
        embed = new Discord.MessageEmbed()
            .setTitle("**Settings**")
            .setDescription(`prefix: ${config.prefix} 
            \nquestion file: ${config.questionFile} 
            \nquestion time: ${config.questionTime} 
            \nsuggest channel: <#${config.suggestChannel}> 
            \nsend channel: <#${config.sendChannel}>
            \nrandom question: ${config.randomQuestion}`)
        
        message.channel.send(embed);
        console.log(`${message.member.displayName} requested the server settings`)
    }
}