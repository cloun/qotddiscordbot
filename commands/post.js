const Discord = require('discord.js');
const config = require('../config.json')
const fs = require('fs');

// copying the randomColour function because I'm too dumb to figure out how to import the function from the main file
function randomColour(colours){
    return colours[Math.floor(Math.random() * colours.length)];
}

module.exports = {
    name: 'post',
    description: 'Forces a question to be send to the send channel and removes it from the queue',
    admin: true,
    execute(client){

        // retrieves all the questions from the question file (whose path is stored in the config)
        // will throw an error if the file doesn't exist
        questions = fs.readFileSync(config.questionFile, "utf8").split('\n');

        // just stores the channel that the messages will be send to so we
        // don't have to keep writing this entire statement over and over again
        sendChannel = client.channels.cache.get(config.sendChannel);

        // if there are no questions in the file then a message is sent to the chat
        // telling users to suggest questions in the suggestions channel
        if(questions.join('').trim() == '') {
            console.log("No questions to ask, message sent to sendChannel");

            errorEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.errorColours))
                .setTitle('**No Questions to Ask**')
                .setDescription(`Ask some questions in <#${config.suggestChannel}> for tommorrow!`)
            
            sendChannel.send(errorEmbed)

            // remakes the questions file because when all the questions are exhausted
            // I end up with two empty lines, easiest way to fix this is to just remake
            // file as a blank txt when this happens
            fs.writeFileSync(config.questionFile, '');
        }
        // otherwise we can proceed with picking the question to send
        else {
            // this is the variable that will store the question to be asked
            question = "";

            // if the config has randomQuestion set to true, a random question will be picked from the array
            // otherwise the questions will be asked in order
            if(config.randomQuestion){
                question = questions.splice(Math.floor(Math.random() * (questions.length - 1)), 1)[0];
            }
            else {
                question = questions.shift();
            }

            // the question is stored as a string with values separated by |
            // the first value is the question, the second is the asker, and the third is the time it was asked
            questionArray = question.split('|');
            //creates the embed that will be sent
            questionEmbed = new Discord.MessageEmbed()
                .setColor(randomColour(config.colours))
                .setTitle("❔❓**Question of the Day**❓❔")
                .setDescription(questionArray[0])
                .setFooter(`Asked by ${questionArray[1]} • Asked on ${questionArray[2]}`);
            
            // the remaining questions will be conjoined together with \n as a separator
            // and then written to the questions file
            // this overwrites whatever is in the questions file currently
            newQuestions = questions.join('\n');
            fs.writeFileSync(config.questionFile, newQuestions);

            // this unpins any messages that the bot may have previously pinned
            sendChannel.messages.fetchPinned()
                .then(pins => {
                    for(pin of [...pins]) {
                        sendChannel.messages.fetch(pin[0])
                            .then(message => {
                                if(message.author.bot){ message.unpin() }
                            })
                    }
                });
            
            // sends the question then pins it
            sendChannel.send(questionEmbed)
                .then(() => {
                    sendChannel.lastMessage.pin();
                });
        }
    }
}