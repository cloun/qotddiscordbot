const Discord = require('discord.js');
const config = require('../config.json')

// copying the randomColour function because I'm too dumb to figure out how to import the function from the main file
function randomColour(colours){
    return colours[Math.floor(Math.random() * colours.length)];
}

module.exports = {
    name: 'help',
    description: `Sends a list of the commands with basic descriptions`,
    admin: false,
    execute(message, args, client){

        // creates the embed that we'll be modifying as we go through the if statements
        embed = new Discord.MessageEmbed()

        // creates an array of all the command files
        commands = client.commands.array();
        
        // Handles the case where the user asks for the admin commands
        if (args[0] == 'admin') {
            // makes sure the user actually has admin perms, if they do
            // the commands are send, if not an error message is sent
            if (message.member.hasPermission('ADMINISTRATOR')) {
                embed.setColor(randomColour(config.colours))
                    .setTitle("**Admin Commands**")
                    .setDescription("these commands require admin permissions to use");
                
                for (command of commands) {
                    if (command.admin) {
                        embed.addField(`${config.prefix}${command.name}`, command.description);
                    }
                }
                
                message.channel.send(embed);
                console.log(`${message.member.displayName} requested the admin commands`);
            } else {
                embed.setColor(randomColour(config.errorColours))
                    .setTitle("**Insufficient Permissions**")
                    .setDescription("Admin permissions are required to see the admin commands")
                    .setFooter("Contact the admins if this is a mistake")
                
                message.channel.send(embed);
                console.log(`${message.member.displayName} required the admin commands with insufficient permissions`);
            }
        }  else {

            embed.setColor(randomColour(config.colours))
                .setTitle("**Commands**")

            for(command of client.commands.array()) {
                if(!command.admin){
                    embed.addField(`${config.prefix}${command.name}`, command.description);
                }
            }

            message.channel.send(embed);
            console.log(`${message.member.displayName} requested a list of commands`);
        }

    }
}