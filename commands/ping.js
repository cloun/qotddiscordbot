module.exports = {
    name: 'ping',
    description: 'Pings the bot',
    admin: true,
    execute(message, args){
        message.channel.send('Pong!');
    }
}