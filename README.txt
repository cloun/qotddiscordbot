clone of an existing question of the day discord bot

accepts questions using <prefix>suggest <question>, asks question at a certain time every day (given in the config), pins the question and removes
any previous pins by the bot. more detailed list of commands with <prefix>commands

all the settings you need to change are in config.json:

prefix (string): sets the prefix for all the bot commands
token (string): your own bot token
questionFile (string): directory pointing to the text file where the questions will be stored (should be in terms of the bot.js file)
questionTime (string): time at which a question will be asked every day. 
                    Uses 24hr time, and reduce each part to the least number of digits, i.e. instead of 14:01:00 for 2:01 pm, just write 14:1:0
suggestChannel (string): channel id of the channel you want the bot to direct users to when it runs out of questions
sendChannel (string): channel id of the channel the bot will send the question of the day in
randomQuestion (boolean): if set to true, the bot will ask a random question from it's queue instead of asking them in chronological order
errorColours (array of strings): array of colour values that will be used when the bot sends error messages in the discord, 
                            each message has a random colour picked from these
colours (array of strings): array of colour values that will be used when the bot sends regular messages in the discord,
                        each message has a random colour picked from these

discord.js required, but it should be included in this repo
first time using node.js and javascript in general, 
if there is something in the bot that can be done more elegantly lmk