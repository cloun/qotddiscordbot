const Discord = require('discord.js');
const config = require('./config.json');

const client = new Discord.Client();
const prefix = config.prefix;

const fs = require('fs');

// Gets all the command files that are stored in the commands folder
// Used the commands folder to store commands that weren't really a part of the main bot
// and to allow for easy addition of commands
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));
for(const file of commandFiles){
    const command = require(`./commands/${file}`);

    client.commands.set(command.name, command);
}

// Function for returning a random colour from the list of colours given
function randomColour(colours){
    return colours[Math.floor(Math.random() * colours.length)];
}

// runs when the client is started
client.once('ready' , () => {
    console.log("QOTD Bot is ready! :D");
    // sets status to the prefix
    client.user.setActivity(`${config.prefix}help`)

    // checks every second if its time to send the question
    setInterval(() => {
        time = new Date();
        dateString = `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`

        if (dateString == config.questionTime) {
            client.commands.get('post').execute(client);
        }
    }, 1000);
})

// handles command sent by users
client.on('message', message => {
    //makes sure the message starts with the prefix and wasn't sent by the bot
    if(!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(' ');
    // retrieves the actual command part of the arguments and puts it in lowercase
    const command = args.shift().toLowerCase();

    // goes through all the possible commands
    switch(command) {
        case 'ping':
            client.commands.get('ping').execute(message, args);
            break;
        case 'suggest':
            client.commands.get('suggest').execute(message, args);
            break;
        case 'post':
            if (message.member.hasPermission("ADMINISTRATOR")) {
                client.commands.get('post').execute(client);
            }
            break;
        case 'queue':
            client.commands.get('queue').execute(message, args);
            break;
        case 'help':
            client.commands.get('help').execute(message, args, client);
            break;
        case 'test':
            if (message.member.hasPermission('ADMINISTRATOR')) {
                client.commands.get('test').execute(message, args, client)
            }
            break;
        case 'remove':
            client.commands.get('remove').execute(message, args);
            break;
        case 'settings':
            if(message.member.hasPermission('ADMINISTRATOR')){
                client.commands.get('settings').execute(message, args);
            } else {
                console.log(`${message.member.displayName} requested server settings with insufficient permissions`)
            }
            break;
        default:
            console.log(`None of the commands matched ${command} sent by ${message.member.displayName}`)
            break;
    }
});

// logs into the server
client.login(config.token);
